﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    class Sylabus
    {
        public string KodPrzedmiotu { get; set; }
        public string RodzajPrzedmiotu { get; set; }
        public string Wydzial { get; set; }
        public string Kierunek { get; set; }
        public string Specjalnosc { get; set; }
        public string PoziomStudiow { get; set; }
        public string ProfilStudiow { get; set; }
        public string FormaStudiow { get; set; }
        public string Rok { get; set; }
        public string Semestr { get; set; }
        public string FormyZajec { get; set; }
        public string Jezyki { get; set; }
        public int ECTS { get; set; }
        public string OsobyProwadzace { get; set; }

    }
}
