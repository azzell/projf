﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Web;
using ClassLibrary1;
using System.ComponentModel.DataAnnotations;

namespace ProjektZPP.Models
{
    
    public class EfektKsztalcenia
    {
        public int Id{ get; set; }
        [Required]
        public string Nazwa { get; set; }
        [Required]
        public string Opis { get; set; }
      
        public GrupaOb grupa { get; set; }
        public string odniesienie { get; set; }

        public virtual ICollection<EfektKsztalceniaDlaPrzedmiotu> EfektyKsztalceniaDlaPrzedmiotu { get; set; }

    }

    public class EfektKsztalceniaDlaPrzedmiotu
    {

        public int Id { get; set; }
        [Required]
        public string Nazwa { get; set; }
        [Required]
        public string Opis { get; set; }

        public virtual ICollection<EfektKsztalcenia> EfektyKierunkowe { get; set; }

    }
}