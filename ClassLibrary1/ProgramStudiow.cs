﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjektZPP.Models
{
    public class ProgramStudiow
    {
        public int LiczbaECTS { get; set; }
        public int LiczbaSem { get; set; }
        public string strukturaStud { get; set; }
        public string TechnikiTworzenia { get; set; }
        public string BezpTech { get; set; }
        public string SposobyWer { get; set; }
        public string OpisWydz { get; set; }
    }
}