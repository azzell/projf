﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjektZPP.Models;

namespace ClassLibrary1
{
    public class ObszarowyEfekt
    {
        public int Id { get; set; }
     
        public string Nazwa { get; set; }

        public string Opis { get; set; }

        public GrupaOb Grupa { get; set; }
        public string odniesienie { get; set; }
    }
}
