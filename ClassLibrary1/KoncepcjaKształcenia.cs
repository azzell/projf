﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjektZPP.Models
{
    public class KoncepcjaKształcenia
    {
        public int Id { get; set; }
        public string FormaStudiów { get; set; }
        public string TytulUzyskany { get; set; }
        public string PrzyporządkowanieObszarówKształcenia { get; set; }
        public string WskazanieDziedzinNauki { get; set; }
        public string WskazanieZwiązkuMisjąUczelni { get; set; }
        public string OgólneCeleKształcenia { get; set; }
        public string OpisKompetencjiOczekiwanych { get; set; }
        public string ZasadyRekrutacji { get; set; }

        public KoncepcjaKształcenia()
        {
            WskazanieDziedzinNauki = "Dziedzina nauk (...) , dyscypliny naukowe : podstawowa dyscyplina naukowa (...), pokrewne dyscypliny naukowe – (...)";
        }
    }
}