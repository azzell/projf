﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjektZPP.Models;

namespace ProjektZPP.Database
{
    public class EfRepository
    {
        private EfDbContext context = new EfDbContext();
        public IEnumerable<EfektKsztalcenia> Efekty { get { return context.Efekty; } }
    }
}