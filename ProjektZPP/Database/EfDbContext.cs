﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ClassLibrary1;
using ProjektZPP.Models;

namespace ProjektZPP.Database
{
    public class EfDbContext:DbContext
    {
        public DbSet<EfektKsztalcenia> Efekty { get; set; }
        public DbSet<EfektKsztalceniaDlaPrzedmiotu> EfektyDlaPrzedmiotu { get; set; }
        public DbSet<ObszarowyEfekt> ObszaroweEfekty { get; set; }
    }
}