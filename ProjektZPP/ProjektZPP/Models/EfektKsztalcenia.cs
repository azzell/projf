﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjektZPP.Models
{
    public enum Grupa
    {
        Wiedza,
        Umiejętności,
        [Display(Name = "Kompetencje Społeczne")]
        Kompetencje_społeczne

    }
    public class EfektKsztalcenia
    {
        public int Id { get; set; }
        public string Symbol { get; set; }
        public string Opis { get; set; }
        public string Obszarowe { get; set; }
        public Grupa Grupa { get; set; }
    }

}