﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjektZPP.Database;

namespace ProjektZPP.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            var db = new EfDbContext();
            var efekty = db.Efekty.ToList();
            return View(efekty);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}