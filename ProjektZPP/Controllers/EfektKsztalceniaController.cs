﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjektZPP.Database;
using ProjektZPP.Models;

namespace ProjektZPP.Controllers
{
    public class EfektKsztalceniaController : Controller
    {

        // GET: EfektKsztalcenia
        public ActionResult EfektKsztalcenia()
        {
            
                using (var db = new EfDbContext())
                {

                var efekty = db.EfektyDlaPrzedmiotu.ToList();
                ViewBag.EfektyDlaPrzedmiotu = efekty
                         .Select(x => new SelectListItem
                         {
                             Value = x.Id.ToString(),
                             Text = x.Nazwa.ToString()

                         });
                    
                }
                return View();
        }
        public void save(EfektKsztalcenia efekt)
        {
            using (var db = new EfDbContext())
            {
                
                db.Efekty.Add(efekt);
                db.SaveChanges();
            }
        }
    }
}